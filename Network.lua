--This file contains functions that are specific to the neural network aspect of the algorithm

buttons = {
    "Up",
    "Down",
    "Left",
    "Right",
}

--This sigmoid function has a range of -1 to 1
function sigmoid(x)
    return 2 / (1 + math.exp(-4.9 * x)) - 1
end

function blankNeuron()
    local neuron = {}

    neuron.incoming = {}
    neuron.value = 0

    return neuron
end

maxNodes = 20000

-- create a network containing a node for each input and output
function generateNetwork(genome)
    local network = {}
    network.neurons = {}

    for i = 1, inputCount do
        network.neurons[i] = blankNeuron()
    end

    for i = 1, outputCount do
        network.neurons[maxNodes + i] = blankNeuron()
    end

    table.sort(genome.genes, function(a, b)
        return (a.outNode < b.outNode)
    end)

    for i = 1, #genome.genes do
        local gene = genome.genes[i]
        if gene.expressed then
            if network.neurons[gene.outNode] == nil then
                network.neurons[gene.outNode] = blankNeuron()
            end
            local neuron = network.neurons[gene.outNode]
            table.insert(neuron.incoming, gene)
            if network.neurons[gene.inNode] == nil then
                network.neurons[gene.inNode] = blankNeuron()
            end
        end
    end
    genome.network = network
end

-- evaluate each neuron using the sigmoid activation function, return the outputs
function evaluateNetwork(network, inputs)
    table.insert(inputs, 1)

    for i = 1, inputCount do
        network.neurons[i].value = inputs[i]
    end

    for _, neuron in pairs(network.neurons) do
        local sum = 0
        for i = 1, #neuron.incoming do
            local incoming = neuron.incoming[i]
            local other = network.neurons[incoming.inNode]
            sum = sum + incoming.weight * other.value
        end

        if #neuron.incoming > 0 then
            neuron.value = sigmoid(sum)
        end
    end

    local outputs = {}
    for i = 1, outputCount do
        local button = "P1 " .. buttons[i]
        if network.neurons[maxNodes + i].value > 0 then
            outputs[button] = true
        else
            outputs[button] = false
        end
    end

    return outputs
end

