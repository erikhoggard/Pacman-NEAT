-- number of genomes to produce in each gen, and number of species to kill off in each gen
populationSize = 200
speciesToRemove = math.floor(populationSize / 20)

-- NEAT compatibility coefficients, used in speciation
disjointCoefficient = 2.0
weightDifferenceCoefficient = 0.5
excessCoefficient = 1.0

-- track innovation numbers, since they are static and unique for each gene
function newInnovation()
    pool.innovation = pool.innovation + 1
    return pool.innovation
end


-- returns a new empty species
function newSpecies()
    local species = {}
    species.topFitness = 0
    species.staleness = 0
    species.genomes = {}
    species.averageFitness = 0

    return species
end

-- create a new generation, remove weak species, then perform speciation on new population
function newGeneration()
    removeWeakGenomes(false)
    rankGlobally()
    removeStaleSpecies()
    rankGlobally()

    for i = 1, #pool.species do
        local species = pool.species[i]
        calculateAverageFitness(species)
    end

    removeWeakSpecies()

    local sum = totalAverageFitness()
    local children = {}
    for i = 1, #pool.species do
        local species = pool.species[i]
        breed = math.floor(species.averageFitness / sum * populationSize) - 1
        for j = 1, breed do
            table.insert(children, breedChild(species))
        end
    end

    removeWeakGenomes(true)

-- create new species until the population is full, then perform speciation
    while #children + #pool.species < populationSize do
        local species = pool.species[math.random(1, #pool.species)]
        table.insert(children, breedChild(species))
    end
    for i = 1, #children do
        local child = children[i]
        addToSpecies(child)
    end

    pool.generation = pool.generation + 1 -- track the generation
end

-- generates a new genome with a NN and mutation rates
function createGenome()
    local genome = {}

    genome.genes = {}
    genome.fitness = 0
    genome.adjustedFitness = 0
    genome.network = {}
    genome.maxneuron = 0
    genome.globalRank = 0

    genome.mutationRates = {}
    genome.mutationRates["connections"] = 0.25
    genome.mutationRates["link"] = 3.0 -- starts above 100%, but can be lowered in time
    genome.mutationRates["bias"] = 0.5
    genome.mutationRates["node"] = 0.5
    genome.mutationRates["enable"] = 0.25
    genome.mutationRates["disable"] = 0.5
    genome.mutationRates["step"] = 0.1

    return genome
end

-- creates a base neural network genome, then mutates it
function basicGenome()
    local genome = createGenome()
    local innovation = 1

    genome.maxneuron = inputCount
    mutate(genome)

    return genome
end

-- generate a new empty gene
function newGene()
    local gene = {}

    gene.inNode = 0
    gene.outNode = 0
    gene.weight = 0
    gene.expressed = true
    gene.innovation = 0

    return gene
end

-- copies a genome for evolution
function copyGenome(genome)
    local genome2 = createGenome()
    for g = 1, #genome.genes do
        table.insert(genome2.genes, copyGene(genome.genes[g]))
    end
    genome2.maxneuron = genome.maxneuron
    genome2.mutationRates["connections"] = genome.mutationRates["connections"]
    genome2.mutationRates["link"] = genome.mutationRates["link"]
    genome2.mutationRates["bias"] = genome.mutationRates["bias"]
    genome2.mutationRates["node"] = genome.mutationRates["node"]
    genome2.mutationRates["enable"] = genome.mutationRates["enable"]
    genome2.mutationRates["disable"] = genome.mutationRates["disable"]

    return genome2
end

-- copy a gene for crossovers
function copyGene(gene)
    local gene2 = newGene()
    gene2.inNode = gene.inNode
    gene2.outNode = gene.outNode
    gene2.weight = gene.weight
    gene2.expressed = gene.expressed
    gene2.innovation = gene.innovation

    return gene2
end

-- counts the number of disjoint genes between two sets of genes.  Used for speciation
function countDisjoints(genes1, genes2)
    local i1 = {}
    for i = 1, #genes1 do
        local gene = genes1[i]
        i1[gene.innovation] = true
    end

    local i2 = {}
    for i = 1, #genes2 do
        local gene = genes2[i]
        i2[gene.innovation] = true
    end

    local disjointGenes = 0
    for i = 1, #genes1 do
        local gene = genes1[i]
        if not i2[gene.innovation] then
            disjointGenes = disjointGenes + 1
        end
    end

    for i = 1, #genes2 do
        local gene = genes2[i]
        if not i1[gene.innovation] then
            disjointGenes = disjointGenes + 1
        end
    end

    local n = math.max(#genes1, #genes2)

    return disjointGenes / n
end


-- calculate the average between the weights of two sets of genes.  Used for speciation
function weightAverage(genes1, genes2)
    local i2 = {}
    for i = 1, #genes2 do
        local gene = genes2[i]
        i2[gene.innovation] = gene
    end

    local sum = 0
    local coincident = 0
    for i = 1, #genes1 do
        local gene = genes1[i]
        if i2[gene.innovation] ~= nil then
            local gene2 = i2[gene.innovation]
            sum = sum + math.abs(gene.weight - gene2.weight)
            coincident = coincident + 1
        end
    end

    return sum / coincident
end

-- decide whether or not two genomes can be classified as belonging to the same species
function isSameSpecies(genome1, genome2)
    local disjointDiff = disjointCoefficient * countDisjoints(genome1.genes, genome2.genes)
    local weightDiff = weightDifferenceCoefficient * weightAverage(genome1.genes, genome2.genes)
    if (disjointDiff + weightDiff < excessCoefficient) then
        return true
    else
        return false
    end
end

-- Rank all of the genomes by fitness
function rankGlobally()
    local global = {}
    for i = 1, #pool.species do
        local species = pool.species[i]
        for j = 1, #species.genomes do
            table.insert(global, species.genomes[j])
        end
    end
    table.sort(global, function(a, b)
        return (a.fitness < b.fitness)
    end)

    for i = 1, #global do
        global[i].globalRank = i
    end
end

-- average fitness of a species
function calculateAverageFitness(species)
    local total = 0

    for i = 1, #species.genomes do
        local genome = species.genomes[i]
        total = total + genome.globalRank
    end

    species.averageFitness = total / #species.genomes
end

-- average fitness between all species
function totalAverageFitness()
    local total = 0
    for i = 1, #pool.species do
        local species = pool.species[i]
        total = total + species.averageFitness
    end

    return total
end

-- remove genomes with low fitness
function removeWeakGenomes(cutToOne)
    for i = 1, #pool.species do
        local species = pool.species[i]

        table.sort(species.genomes, function(a, b)
            return (a.fitness > b.fitness)
        end)

        local remaining = math.ceil(#species.genomes / 2)
        if cutToOne then
            remaining = 1
        end
        while #species.genomes > remaining do
            table.remove(species.genomes)
        end
    end
end

-- create a new genome by crossing over genes from two parent genomes and adding mutations
function breedChild(species)
    local child = {}

    if math.random() < 0.75 then --75% chance of crossovers
        g1 = species.genomes[math.random(1, #species.genomes)]
        g2 = species.genomes[math.random(1, #species.genomes)]
        child = crossover(g1, g2)
    else
        g = species.genomes[math.random(1, #species.genomes)]
        child = copyGenome(g)
    end

    mutate(child)

    return child
end

-- Remove the species that have stayed around for too long
function removeStaleSpecies()
    local survived = {}

    for s = 1, #pool.species do
        local species = pool.species[s]

        table.sort(species.genomes, function(a, b)
            return (a.fitness > b.fitness)
        end)

        if species.genomes[1].fitness > species.topFitness then
            species.topFitness = species.genomes[1].fitness
            species.staleness = 0
        else
            species.staleness = species.staleness + 1
        end
        if species.staleness < speciesToRemove or species.topFitness >= pool.maxFitness then
            table.insert(survived, species)
        end
    end

    pool.species = survived
end

-- remove a species if its fitness significantly lower than average
function removeWeakSpecies()
    local survived = {}

    local sum = totalAverageFitness()
    for i = 1, #pool.species do
        local species = pool.species[i]
        breed = math.floor(species.averageFitness / sum * populationSize)
        if breed >= 1 then
            table.insert(survived, species)
        end
    end

    pool.species = survived
end

-- assign a child to a species
function addToSpecies(child)
    local foundSpecies = false
    for i = 1, #pool.species do
        local species = pool.species[i]
        if not foundSpecies and isSameSpecies(child, species.genomes[1]) then
            table.insert(species.genomes, child)
            foundSpecies = true
        end
    end

-- if the child doesn't belong to a species, create a new species for them
    if not foundSpecies then
        local childSpecies = newSpecies()
        table.insert(childSpecies.genomes, child)
        table.insert(pool.species, childSpecies)
    end
end






