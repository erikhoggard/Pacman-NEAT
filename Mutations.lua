
--mutate genes, and change individual mutation rates
function mutate(genome)
    for mutation, rate in pairs(genome.mutationRates) do
        if math.random(1, 2) == 1 then
            genome.mutationRates[mutation] = 0.95 * rate
        else
            genome.mutationRates[mutation] = 1.05 * rate
        end
    end

    if math.random() < genome.mutationRates["connections"] then
        weightMutation(genome)
    end

    local p = genome.mutationRates["link"]
    while p > 0 do
        if math.random() < p then
            addConnectionMutation(genome, false)
        end
        p = p - 1
    end

    p = genome.mutationRates["bias"]
    while p > 0 do
        if math.random() < p then
            addConnectionMutation(genome, true)
        end
        p = p - 1
    end

    p = genome.mutationRates["node"]
    while p > 0 do
        if math.random() < p then
            addNodeMutation(genome)
        end
        p = p - 1
    end

    p = genome.mutationRates["enable"]
    while p > 0 do
        if math.random() < p then
            toggleMutation(genome, true)
        end
        p = p - 1
    end

    p = genome.mutationRates["disable"]
    while p > 0 do
        if math.random() < p then
            toggleMutation(genome, false)
        end
        p = p - 1
    end
end

-- cross over genes from a more fit genome to a less fit genome
function crossover(g1, g2)
    if g2.fitness > g1.fitness then
        g1, g2 = g2, g1
    end

    local child = createGenome()

    local innovations2 = {}
    for i = 1, #g2.genes do
        local gene = g2.genes[i]
        innovations2[gene.innovation] = gene
    end

    for i = 1, #g1.genes do
        local gene1 = g1.genes[i]
        local gene2 = innovations2[gene1.innovation]
        if gene2 ~= nil and math.random(2) == 1 and gene2.expressed then
            table.insert(child.genes, copyGene(gene2))
        else
            table.insert(child.genes, copyGene(gene1))
        end
    end

    child.maxneuron = math.max(g1.maxneuron, g2.maxneuron)

    for mutation, rate in pairs(g1.mutationRates) do
        child.mutationRates[mutation] = rate
    end

    return child
end

-- get a random gene from a set, isInput determines whether or not it has to be an input neuron
function randomNeuron(genes, isInput)
    local neurons = {}
    if isInput then
        for i = 1, inputCount do
            neurons[i] = true
        end
    end
    for i = 1, outputCount do
        neurons[maxNodes + i] = true
    end
    for i = 1, #genes do
        if (isInput) or genes[i].inNode > inputCount then
            neurons[genes[i].inNode] = true
        end
        if (isInput) or genes[i].outNode > inputCount then
            neurons[genes[i].outNode] = true
        end
    end

    local count = 0
    for _, _ in pairs(neurons) do
        count = count + 1
    end
    local n = math.random(1, count)

    for k, v in pairs(neurons) do
        n = n - 1
        if n == 0 then
            return k
        end
    end

    return 0
end


-- mutate the weights for a gene
function weightMutation(genome)
    local step = genome.mutationRates["step"]

    for i = 1, #genome.genes do
        local gene = genome.genes[i]
        if math.random() < 0.90 then
            gene.weight = gene.weight + math.random() * step * 2 - step
        else
            gene.weight = math.random() * 4 - 2
        end
    end
end

-- connect two unconnected nodes
function addConnectionMutation(genome, forceBias)
    local node1 = randomNeuron(genome.genes, true)
    local node2 = randomNeuron(genome.genes, false)
    local newLink = newGene()

    if node2 <= inputCount and node1 <= inputCount then
        return
    end

    if node1 <= inputCount then
        node1, node2 = node2, node1
    end
    newLink.inNode = node2
    newLink.outNode = node1
    if forceBias then
        newLink.inNode = inputCount
    end

-- if already connected don't add a connection
    for i = 1, #genome.genes do
        if genome.genes[i].inNode == newLink.inNode and genome.genes[i].outNode == newLink.outNode then
            return
        end
    end


    newLink.innovation = newInnovation()
    newLink.weight = math.random() * 4 - 2

    table.insert(genome.genes, newLink)
end

-- add a node to the network
function addNodeMutation(genome)
    if #genome.genes == 0 then
        return
    end

    genome.maxneuron = genome.maxneuron + 1

    local gene = genome.genes[math.random(1, #genome.genes)]
    if not gene.expressed then
        return
    end

    gene.expressed = false

    local gene1 = copyGene(gene)
    gene1.outNode = genome.maxneuron
    gene1.weight = 1.0
    gene1.innovation = newInnovation()
    gene1.expressed = true
    table.insert(genome.genes, gene1)

    local gene2 = copyGene(gene)
    gene2.inNode = genome.maxneuron
    gene2.innovation = newInnovation()
    gene2.expressed = true
    table.insert(genome.genes, gene2)
end

-- toggle a gene on or off without deleting it
function toggleMutation(genome, enable)
    local candidates = {}
    for _, gene in pairs(genome.genes) do
        if gene.expressed == not enable then
            table.insert(candidates, gene)
        end
    end

    if #candidates == 0 then
        return
    end

    local gene = candidates[math.random(1, #candidates)]
    gene.expressed = not gene.expressed
end



