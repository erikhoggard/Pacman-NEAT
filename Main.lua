require "Network"
require "Evolution"
require "Mutations"

savestateFName = "start.state"
inputSize = 9; --input is a grid of tiles centered on pacman.
inputCount = ((2 * inputSize + 1) ^ 2) + 1
outputCount = #buttons --outputs are the buttons of the NES controller

print(memory.getmemorydomainlist())
memory.usememorydomain("CIRAM (nametables)")

ghostNear = false

score = 0;
bestScore = 0;
lifeCount = 0;
pelletCount = 0;

pacmanX = 0;
pacmanY = 0;
pacmanTile = 0;

blinkyX = 0;
blinkyY = 0;
blinkyTile = 0;
blinkySprite = 0;

pinkyX = 0;
pinkyY = 0;
pinkyTile = 0;
pinkySprite = 0;

inkyX = 0;
inkyY = 0;
inkyTile = 0;
inkySprite = 0;

clydeX = 0;
clydeY = 0;
clydeTile = 0;
clydeSprite = 0;


-- Check RAM and update positions of pacman and the ghosts
function updateCharacterPositions()

    memory.usememorydomain("RAM")

    pelletCount = memory.readbyte(0x006A)
    lifeCount = memory.readbyte(0x0067)

    pacmanX = memory.readbyte(0x001A)
    pacmanY = memory.readbyte(0x001C)
    if ((pacmanY - 16) % 8 == 0) then
        memory.usememorydomain("RAM")
        pacmanTile = getTileAtCoords(pacmanX, pacmanY)
    end

    memory.usememorydomain("RAM")
    blinkyX = memory.readbyte(0x001E)
    blinkyY = memory.readbyte(0x0020)
    blinkySprite = memory.readbyte(0x0033)
    if ((blinkyY - 16) % 8 == 0) then
        blinkyTile = getTileAtCoords(blinkyX, blinkyY)
    end

    memory.usememorydomain("RAM")
    pinkyX = memory.readbyte(0x0022)
    pinkyY = memory.readbyte(0x0024)
    pinkySprite = memory.readbyte(0x0034)
    if ((pinkyY - 16) % 8 == 0) then
        pinkyTile = getTileAtCoords(pinkyX, pinkyY)
    end

    memory.usememorydomain("RAM")
    inkyX = memory.readbyte(0x0026)
    inkyY = memory.readbyte(0x0028)
    inkySprite = memory.readbyte(0x0035)
    if ((inkyY - 16) % 8 == 0) then
        inkyTile = getTileAtCoords(inkyX, inkyY)
    end

    memory.usememorydomain("RAM")
    clydeX = memory.readbyte(0x002A)
    clydeY = memory.readbyte(0x002C)
    clydeSprite = memory.readbyte(0x0036)
    if ((clydeY - 16) % 8 == 0) then
        clydeTile = getTileAtCoords(clydeX, clydeY)
    end
end

-- translate pixel coordinates into the RAM address of the corresponding tile
function getTileAtCoords(x, y)

    if (y == 16 or (y - 16) % 8 == 0) then

        local base = 98 -- The top-leftmost walkable tile has a memory address of 98
        local tileSize = 8 -- 8 distance units between tiles, horizontally and vertically
        local rowOffsetMult = 32 -- There are 32 bytes between rows of tiles in memory
        local xOffset = 24 -- The top-leftmost walkable position of pacman/ghosts has an x position of 24
        local yOffset = 16 -- The top-leftmost walkable position of pacman/ghosts has a  y position of 16

        local row = base + ((y - yOffset) / tileSize) * rowOffsetMult
        local column = (x - xOffset) / tileSize

        local memAddress = math.floor(row + column)

        return memAddress
    end
end

-- get the contents of the tile (wall, dot, empty space, etc.)
function getTileValue(address)
    memory.usememorydomain("CIRAM (nametables)")
    tile = memory.readbyte(address)
    return tile
end

function genInputs()
    updateCharacterPositions()

    local inputList = {}

    for i = -9, 9, 1 do
        for j = -9, 9, 1 do
            inputList[#inputList + 1] = 0
            local x = pacmanX + (j * 8)
            local y = pacmanY + (i * 8)

            if (x >= 24 and x <= 168 and y >= 16 and y <= 208) then
                if ((x - 24) % 8 == 0 and (y - 16) % 8 == 0) then
                    local addr = getTileAtCoords(x, y)
                    local tile = getTileValue(addr)
                    -- detect empty walkable space
                    if (tile == 7 or tile == 0) then
                        inputList[#inputList] = 0.75
                    end
                    -- detect dots
                    if (tile == 3) then
                        inputList[#inputList] = 1
                    end
                    if (tile == 1 or tile == 2) then
                        inputList[#inputList] = 1
                    end
                    -- detect ghosts
                    ghostNear = false
                    if ((addr == blinkyTile and (blinkySprite ~= 30 and blinkySprite ~= 31)) or
                            (addr == pinkyTile and (pinkySprite ~= 30 and pinkySprite ~= 31)) or
                            (addr == inkyTile and (inkySprite ~= 30 and inkySprite ~= 31)) or
                            (addr == clydeTile and (clydeSprite ~= 30 and clydeSprite ~= 31))) then
                        inputList[#inputList] = -1

                    else
                        if ((addr == blinkyTile and (blinkySprite == 30 and blinkySprite == 31)) or
                                (addr == pinkyTile and (pinkySprite ~= 30 and pinkySprite == 31)) or
                                (addr == inkyTile and (inkySprite == 30 and inkySprite == 31)) or
                                (addr == clydeTile and (clydeSprite == 30 and clydeSprite == 31))) then
                            inputList[#inputList] = 1
                        end
                    end
                end
            end
        end
    end

    return inputList
end

function clearButtons()
    controller = {}
    for b = 1, #buttons do
        controller["P1 " .. buttons[b]] = false
    end
    joypad.set(controller)
end


function evaluateInputs()
    local species = pool.species[pool.currentSpecies]
    local genome = species.genomes[pool.currentGenome]

    inputs = genInputs()
    controller = evaluateNetwork(genome.network, inputs)

--make sure left/right and up/down aren't pressed simultaneously
    if controller["P1 Left"] and controller["P1 Right"] then
        controller["P1 Left"] = false
        controller["P1 Right"] = false
    end
    if controller["P1 Up"] and controller["P1 Down"] then
        controller["P1 Up"] = false
        controller["P1 Down"] = false
    end

    joypad.set(controller)
end


function nextGenome()
    pool.currentGenome = pool.currentGenome + 1
    if pool.currentGenome > #pool.species[pool.currentSpecies].genomes then
        pool.currentGenome = 1
        pool.currentSpecies = pool.currentSpecies + 1
        if pool.currentSpecies > #pool.species then
            newGeneration()
            pool.currentSpecies = 1
        end
    end
end

function fitnessAlreadyMeasured()
    local species = pool.species[pool.currentSpecies]
    local genome = species.genomes[pool.currentGenome]

    return genome.fitness ~= 0
end

function initializeRun()
    savestate.load(savestateFName);
    bestScore = 0
    pool.currentFrame = 0
    timeout = 60
    clearButtons()

    local species = pool.species[pool.currentSpecies]
    local genome = species.genomes[pool.currentGenome]

    generateNetwork(genome)
    evaluateInputs()
end

-- replay the best performing network
function replay()
    --TODO
    print('not currently implemented')
end

if pool == nil then
    pool = {}
    pool.species = {}
    pool.generation = 0
    pool.innovation = outputCount
    pool.currentSpecies = 1
    pool.currentGenome = 1
    pool.currentFrame = 0
    pool.maxFitness = 0

    for i = 1, populationSize do
        basic = basicGenome()
        addToSpecies(basic)
    end

    initializeRun()
end

-- program loop - this code will run once per frame
while true do

    local species = pool.species[pool.currentSpecies]
    local genome = species.genomes[pool.currentGenome]

    -- evaluate frames 10 times per second.  NES runs at 60fps
    if pool.currentFrame % 6 == 0 then
        evaluateInputs()
    end

    joypad.set(controller)
    updateCharacterPositions()
    if 192 - pelletCount > bestScore then
        bestScore = 192 - pelletCount
        timeout = 20 + 10 * (192 - pelletCount)
    end

    timeout = timeout - 1

    gui.drawText(175, 90, "gen:" .. pool.generation)
    gui.drawText(175, 105, "species:" .. pool.currentSpecies)
    if(pool.currentGenome > 1) then
        gui.drawText(175, 120, "genome:" .. pool.currentGenome -1)
    else
        gui.drawText(175, 120, "genome:" .. pool.currentGenome)
    end
    gui.drawText(175, 150, "top:" .. pool.maxFitness)
    gui.drawText(175, 165, "Q - replay")

-- increase timeout depending on how long the run lasts for
    if timeout +  pool.currentFrame / 2 <= 0 or lifeCount < 3 then

        local fitness = bestScore * 10

--        if lifeCount < 3 then
--            fitness = -1
--        end

        if fitness == 0 then
            fitness = -1
        end
        genome.fitness = fitness

        if fitness > pool.maxFitness then
            pool.maxFitness = fitness
        end

        pool.currentSpecies = 1
        pool.currentGenome = 1

        while fitnessAlreadyMeasured() do
            nextGenome()
        end
        initializeRun()
    end

    local measured = 0
    local total = 0
    for _, species in pairs(pool.species) do
        for _, genome in pairs(species.genomes) do
            total = total + 1
            if genome.fitness ~= 0 then
                measured = measured + 1
            end
        end
    end


    local keys = input.get()
    if(keys['Q'] == true) then
        replay()
    end

    pool.currentFrame = pool.currentFrame + 1
    emu.frameadvance();
end